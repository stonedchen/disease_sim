---
title: "Handwritten MCMC"
output: html_notebook
---
```{r helpers}

rbern <- function(n, p) {
    rbinom(n, 1, p)
}


```

```{r simulations}
#

# start to simulate data

####
N <- 199
t <- 1:N
gamma  <- c(log(10), 0.7, 1.7)

K <- 3
theta  <- c(30, 50, 150)

lambda <- c(0.7, 1.15, 0.7, 0.3)
vlambda <- rep(lambda, diff(c(0, theta, N)))

rho <- 2*pi/52
nu <- exp(cbind(1, sin(rho*t), cos(rho*t)) %*% gamma)

Z <- numeric(N+1)
Z[1] <- 1


X <- rpois(1:N, nu)

for(i in 2:(N+1)) {
  Z[i] <- X[i-1] + rpois(1, vlambda[i-1] * Z[i-1])
}


plot(Z, type = "l")
###
#end of simulation
###
```


```{r}
## update number of change points theta


update_theta <- function(theta, N){

  K <- length(theta)                     #find the current length
  
   if(K <= 1) {                           #if it's less than 1, always add
    sort(c(theta, sample(1:N, 1))) 

  } else if (K >= N - 2) {               #else if it's max always remove
    sort(sample(theta, K - 1))

  } else if (rbern(1, 0.5) == 1) {      #if not edge case, sub or...
    sort(sample(theta, K - 1))

  } else {
    sort(c(theta, sample(setdiff(1:N, theta), 1)))  #add

  }

}


## call update_theta then update number of change points K
## and lambda
## then use LL to accept or reject

 

update_param <- function(K, N, theta, lambda, LL) {

  theta_star  <- update_theta(theta, N)

  lambda_star <- rf(length(theta_star) + 1, 2, 20) #f-dist like the paper
 # need to adjust hastings-ratio
  vlambda <- rep(lambda_star, diff(c(0, theta_star, N))) #make into vector

  LL_star <- sum(dpois(Z, nu + vlambda*Z[-(N-1)], log = T)) #likelihood

  if(LL_star > LL) {

    list(K = length(theta_star), N = N, theta = theta_star, lambda = lambda_star, LL = LL_star)

  } else if (exp(LL_star - LL) > runif(1)) {

    list(K = length(theta_star), N = N, theta = theta_star, lambda = lambda_star, LL = LL_star)

  } else { 

    list(K = length(theta), N = N, theta = theta, lambda = lambda, LL = LL)

  }

}

 

nSim <- 1000000

#pre-allocate
res <- list(K = numeric(nSim),
            theta = matrix(0, nSim, N - 2),
            lambda = matrix(0, nSim, N))

 

output <- list(K = K, N= N, theta = theta, lambda = lambda, LL = -Inf)

for(i in 1:nSim){
   #i <- 1
   output <-  update_param(output$K, N, output$theta, output$lambda, output$LL)
   k <- output$K
   res$K[i] <- k
   res$theta[i, ] <- c(output$theta, rep(output$theta[k], N - 2 - k))
   res$lambda[i, ] <- c(output$lambda, rep(output$lambda[k+1], N - k - 1))
}

 


```
