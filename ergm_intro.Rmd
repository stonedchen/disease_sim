---
title: "ERGMs and ergm packages"
output: 
  html_notebook:
    toc: true
    theme: tufte
bibliography: disease_sim_proj.bib
link-citations: yes
header-includes:
   - \usepackage{mathrsfs}
---
# Simulation stuff
```{r}
g.sim <- simulate(network(16) ~ edges + mutual)

g.sim <- simulate(network(16) ~ edges + mutual, coef=c(0,2))
summary(g.sim ~ edges + mutual)
plot(g.sim)

g.use <- network(16,density=0.1,directed=FALSE)

g.sim <- simulate(~edges+kstar(2),nsim=5,coef=c(-1.8,0.03),
               basis=g.use,
               burnin=100000,interval=1000)
g.si

gest <- ergm(flomarriage ~ edges + kstar(2))
summary(gest)
```

```{r}
get.neighborhood(g.sim, 1)

g.sim[,]
```


# graph models general

From @kolaczyk_statistical_2014, @kolaczyk_statistical_2009

All of this is for a FIXED graph size

In general we need to specify

$$ \{P_\theta(G), G \in \mathscr{G}: \theta \in \Theta\} $$
## Bernoulli

$$\{P(G) = \binom{N}{N_e}^{-1}: G \in \mathscr{G}_{N_v,N_e} \}$$

That is the probability is 1 over the number of ways to select that many edges.

$$ \{P_\theta(G), G \in \mathscr{G}: \theta \in \Theta\} $$

Consider the task of estimating a given characteristic η (G) of a network graph G,
based on a sampled version of that graph, G ∗ . In Chapter 5, we presented almost ex-
clusively a design-based perspective on this problem. Alternatively, we might aug-
ment this perspective to included a model-based component as well, wherein G is
assumed to have been generated uniformly at random from a collection G, prior to
our obtaining G ∗ . Inference on η then should incorporate both randomness due to
selection of G from G and randomness due to sampling G ∗ from G. We will illus-
trate this approach for estimating the size of a ‘hidden’ population, using one of the
models introduced in this section, later in Section 6.2.4

## graphs are the same size

https://arxiv.org/pdf/1904.10406.pdf

##notation ergm


Notation:

* $Y_{ij} = Y_{ji}$ be a binary random variable 
* $H$ is a configuration a set of possible edges in the graph
* $g_H(y) = \Pi_{y_{ij\in H}}y_{ij}$ 1 if configuration occurs or 0 otherwise.

* non-zero $\theta_H$ means $Y_{ij}$ are dependent for all pairs of vertices $\{i,j\}$ in $H$, conditional upon the rest of the graph

* $k = k(\theta)$ is a normalization constant

$$k(\theta) = \sum_y \exp{\sum_H \theta_H\ g_H(y)}$$

$$P_\theta(Y = y) = \frac{1}{k}\exp\{\sum_H\theta_H\ g_H(y)\}$$


# ERGM package
```{r}
library(statnet)
```


## From Vignette

$$Pr(Y = y) = \exp[\theta 'g(y)]/k(\theta)$$

Notation:

* $Y$ is a network
* $g(y)$ is a vector of network statistics
* $\theta$ is a vector of coefficients
* $k(\theta)$ is a normalizing constant

$$ \text{logit(Pr}(Y_{ij}=1|Y^c) = \theta ' \Delta(g(y))_{ij} $$

Notation

* $Y_{ij}$ is an actor-pair in Y
* $\Delta(g(y))_{ij}$ is the change in $g(y)$ when $Y_{ij}$ is toggled on




```{r}
data(florentine)
flomarriage
plot(flomarriage)
```
```{r}
flomodel.01 <- ergm(flomarriage~edges)
```

```{r}
summary(flomodel.01)

lo2p <- function(logodds){
  exp(logodds)/(1 + exp(logodds))
}

lo2p(-1.6094)

```
```{r}
flomodel.02 <- ergm(flomarriage~edges+triangle)
```

