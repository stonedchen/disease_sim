---
title: "Disease Modeling"
output: 
  html_notebook:
    toc: true
bibliography: disease_sim_proj.bib
link-citations: yes
---



# Introduction



A stochastic model for statistical analysis of surveillance data of infection disease counts.

Data has seasonalities and outbreaks, low counts are typical so normal approximations are often inadequate 

allow for overdispersion
be non-stationary

mechanistic modeling - susceptible, exposed, infectious removed - is too ambitious eg information of susceptibles, under-reporting and reporting delays

surveillance data have features that are not captured by empirical models like log-linear poisson

Our starting point is a simple branching process model with autoregressive parameter $\lambda$ and Poisson offspring 

* viewed as approximation to the chain-binomial model
* this mechanistic model assume that time period between data is generation time between generations of infected

* $\lambda$ is therefore not the basic reproduction number

* allows for endemic count, so the #will not explode or die out with probability 1

* include seasonal terms for endemic rate

* replace poisson with negative binomial to allow over-dispersion

* The additional influx of endemic cases implies that whenever λ≥1 an outbreak will occur, while for λ<1 the process will be stable. 

* Inclusion of overdispersion through latent random effects can be seen as an attempt to adjust for unobserved covariates or mechanisms that affect the disease incidence. For example, overdispersion can be caused by the fact that the generation time of many infectious diseases does not equal the time unit in which the data are collected or simply by the influence of unobserved covariates that affect the disease incidence.

A central feature is to let the auto-regressive threshold parameter of the branching process vary over time

* infectiousness might change due to public health measures, increasing vaccination

* when the number of susceptible decreases (number of infected is up but lower amounts of targets)


Want smooth change in $\lambda$, also want to capture sudden changes in infectiousness

* state-space or dynamic model with autoregressive or random walk prior on lambda does not allow for sudden changes

* a bayesian change-point model with unknown number of change points

* Through Bayesian model averaging, the estimated time-changing λ may still be smooth because it is obtained through averaging over different changepoint models of variable dimension with different locations of the changepoints (Green, 1995; Clyde, 1999).


# Model

Let $Z = (Z_1, ..., Z_n)$ be the weekly counts of infectious diseases. The model is specified through $Z_t | Z_{t-1}$.

$Z_t$ follows a Poisson branching process with immigration and time-varying parameters $\lambda_t$ and $v_t$.

$$ Z_t = X_t + Y_t$$
$$ X_t \sim Pois(v_t)$$
$$Y_t|Z_{t-1} \sim Pois(\lambda_tZ_{t-1})$$

## Code template

1. Change point model
    a. need to simulate change points of $\lambda$; "smooth"?
       * num of change points is uniformly distributted
       * num of location of change points is uniformly distributed
    b. need to get distributions of $\lambda^{(k)}$
      *  iid exponential $1/\zeta$
      *  gamma prior on inverse mean
      
      
# Two-Component Simulation

```{r change point model}


n = 199 #total epochs/time steps
ende_lambda = rep(0.5, n) #vector of endemic lambda values (\nu)
epi_lambda = rep(0.5, n)  #vector of epidemic lambda values
init = 10   #Z_0 value is the initial number of people infected


#ende_lambda = rep(0.5, n) #vector of endemic lambda values (\nu)

#---simulate endemic component based on paper's values----#
rho = 2*pi/52
gamma_0 = log(10)
gamma_1 = 0.5
gamma_2 = 1.5


nu_t = gamma_0 + gamma_1*sin(rho*(1:n)*(1+1)/2) + gamma_2*cos(rho*(1:n)*(2+1)/2)

ende_lambda = exp(nu_t)



#----ability to do random change point-----#
exp_rate = 2 #prior for the lambda value in the poisson process
k = sample(1:n, 1)
chg_pt_idx = sort(sample(1:n, k))

epi_lambda[1:(chg_pt_idx[1]-1)] = rexp(1, exp_rate)

for (i in 1:(length(chg_pt_idx)-1)) {
    epi_lambda[chg_pt_idx[i]:chg_pt_idx[i+1]] = rexp(1, exp_rate)
}

#-----setting epidemic lambda values 
epi_lambda = rep(c(0.7, 1.2, 0.7), c(39,10,150))



counts = rep(0, n) #initialize count vector
counts[1] = init #initial value

ende = rep(0, n)
epi = rep(0, n)

for (i in 1:n) {
  ende[i] = rpois(1, ende_lambda[i])
  epi[i] = rpois(1, epi_lambda[i]*counts[i])
  
  counts[i+1] = ende[i] + epi[i]
}

plot(counts, type = "l")
lines(ende, type = "l", col = "red")
lines(epi, type = "l", col = "blue")
```


# Graph Simulation outline

* fix a graph, say 5 nodes, with a certain connectivity
* like one completely central, the rest are branches


```{r}
#library(igraph)

complete_5 = sample_gnp(5, 1)
plot(complete_5)


el = matrix(
  c(1,2,1,3,1,4,1,5,1,6, 2, 7, 2, 8, 7,8, 6,9, 9, 10),
  byrow = TRUE,
  ncol = 2)

g_city = graph_from_edgelist(el, directed = FALSE)
plot(g_city)


neighbors(g_city, 1)
verts = adjacent_vertices(g_city, c(1,2))

for (i in verts[[1]]) {
  print(i+1)
}
length(g_city)

ts_data = matrix(0, nrow = length(g_city), ncol = n)

ts_data[verts[[1]], 1] = 10
verts[[1]]
```

## Complete Graph version

Basically need to create a new counts matrix, and then use the adjacency from the graph to determine which counts to include when computing the counts for each city

Need to adjust each lambda parameter down since we're now summing poissons. Divide $$\lambda$$ by number of cities.

eg

* og count of 5 will result in $$pois(\lambda*5)$$
* in the fully connected graph each city will see 5 counts, and then decide accordingly

$$Pois_1, Pois_2, Pois_3,..., Pois_{city}$$


```{r}
n = 199 #total epochs/time steps
init = 10   #Z_0 value is the initial number of people infected
graph = sample_gnp(10, 1) #speed check

#---simulate endemic component based on paper's values----#
#-----------NOTE: need to divide the rate since each city gets a chance
rho = 2*pi/52
gamma_0 = log(10)
gamma_1 = 0.5
gamma_2 = 1.5


nu_t = gamma_0 + gamma_1*sin(rho*(1:n)*(1+1)/2) + gamma_2*cos(rho*(1:n)*(2+1)/2)


#need to divide!
ende_lambda = rep(0, n)
ende_lambda = exp(nu_t)/gorder(graph)

epi_lambda = rep(c(0.7, 1.2, 0.7), c(39,10,150))


#----ability to do random change point-----#
exp_rate = 5 #prior for the lambda value in the poisson process
k = sample(1:n, 1)
chg_pt_idx = sort(sample(1:n, k))

epi_lambda[1:(chg_pt_idx[1]-1)] = rexp(1, exp_rate)

for (i in 1:(length(chg_pt_idx)-1)) {
    epi_lambda[chg_pt_idx[i]:chg_pt_idx[i+1]] = rexp(1, exp_rate)
}

#-----setting epidemic lambda values 
epi_lambda = rep(c(0.7, 1.2, 0.7)/gorder(graph), c(39,10,150))






#----separate count figures----#
ende_c = matrix(0, nrow = gorder(graph), ncol = n)
epi_c = matrix(0, nrow = gorder(graph), ncol = n)



#----------------

#-----------------counts matrix ------



counts = matrix(0, nrow = gorder(graph), ncol = n)
counts[1,1] = init

#counts = rep(0, n) #initialize count vector
#counts[1] = init #initial value
 

#get list of all adjacent vertices
adj = adjacent_vertices(graph, 1:gorder(graph))



#-----------

for (i in 1:(n-1)) {
  #all rows/cities get same value
  ende_c[, i] = rpois(1, ende_lambda[i])
  
  for (j in 1:gorder(graph)) {
    counts_j = sum(counts[adj[[j]], i]) #sum all adjacent counts
    counts_j = counts_j + counts[j, i] #add self count
    epi_c[j, i] = rpois(1, epi_lambda[i] * counts_j) #generate new counts
    counts[j , i+1] = ende_c[j, i] + epi_c[j, i] #append new counts
  }
  
  
}

plot(graph)

plot(colSums(counts), type = 'l')
lines(colSums(ende_c), col = 'blue', type = 'l')
lines(colSums(epi_c), col = 'red', type = 'l')
title(main = "all cities sum")

plot(counts[1, ], type = 'l', col = 'forestgreen')
lines(ende_c[1, ], type = 'l', col = 'blue')
lines(epi_c[1, ], type = 'l', col = 'red')
title(main = 'city 1')

plot(counts[10, ], type = 'l', col = 'forestgreen')
lines(ende_c[10, ], type = 'l', col = 'blue')
lines(epi_c[10, ], type = 'l', col = 'red')
title(main = 'city 10')


```
## City graph

```{r}
n = 199 #total epochs/time steps
init = 10   #Z_0 value is the initial number of people infected
graph = g_city

#---simulate endemic component based on paper's values----#
#-----------NOTE: need to divide the rate since each city gets a chance
rho = 2*pi/52
gamma_0 = log(10)
gamma_1 = 0.5
gamma_2 = 1.5


nu_t = gamma_0 + gamma_1*sin(rho*(1:n)*(1+1)/2) + gamma_2*cos(rho*(1:n)*(2+1)/2)


#need to divide!
#ende_lambda = rep(0, n)       
ende_lambda = exp(nu_t)/3.5  #lambda values



#----ability to do random change point-----#
exp_rate = 5 #prior for the lambda value in the poisson process
k = sample(1:n, 1)
chg_pt_idx = sort(sample(1:n, k))

epi_lambda[1:(chg_pt_idx[1]-1)] = rexp(1, exp_rate)

for (i in 1:(length(chg_pt_idx)-1)) {
    epi_lambda[chg_pt_idx[i]:chg_pt_idx[i+1]] = rexp(1, exp_rate)
}

#-----setting epidemic lambda values 
epi_lambda = rep(c(0.7, 1.2, 0.7)/3.5, c(39,10,150))






#----separate count figures----#
ende_c = matrix(0, nrow = gorder(graph), ncol = n)
epi_c = matrix(0, nrow = gorder(graph), ncol = n)



#----------------

#-----------------counts matrix ------



counts = matrix(0, nrow = gorder(graph), ncol = n)
counts[1,1] = init

#counts = rep(0, n) #initialize count vector
#counts[1] = init #initial value
 

#get list of all adjacent vertices
adj = adjacent_vertices(graph, 1:gorder(graph)) #



#-----------

for (i in 1:(n - 1)) {
  #all rows/cities get same value
  ende_c[, i] = rpois(1, ende_lambda[i])
  
  for (j in 1:gorder(graph)) {
    counts_j = sum(counts[adj[[j]], i]) #sum all adjacent counts
    counts_j = counts_j + counts[j, i] #add self count
    epi_c[j, i] = rpois(1, epi_lambda[i] * counts_j) #generate new counts
    counts[j , i + 1] = ende_c[j, i] + epi_c[j, i] #append new counts
  }
  
  
}

plot(graph)

plot(colSums(counts), type = 'l')
lines(counts[1,], col = 'blue', type = 'l')
lines(counts[10,], col = 'red', type = 'l')
lines(counts[7, ], col = "forestgreen", type = 'l')
legend(100,2000, legend = c("total", "city 1 (deg 5)", "city 10 deg(1)", "city 7 (d2, triangle)"), col = c("black", "blue", "red", "forestgreen"), lty = 1)

plot(colSums(counts), type = 'l')
lines(colSums(ende_c), col = 'blue', type = 'l')
lines(colSums(epi_c), col = 'red', type = 'l')
title(main = "all cities sum")

plot(counts[1, ], type = 'l', col = 'black')
lines(ende_c[1, ], type = 'l', col = 'blue')
lines(epi_c[1, ], type = 'l', col = 'red')
title(main = 'city 1')

plot(counts[10, ], type = 'l', col = 'black')
lines(ende_c[10, ], type = 'l', col = 'blue')
lines(epi_c[10, ], type = 'l', col = 'red')
title(main = 'city 10')



#----compute likelihoods

library(igraph)

ord = gorder(graph) #the number of vertices/cities/rows
N = n #number of columns

#get adjacency list
adj = adjacent_vertices(graph, 1:gorder(graph))

inf_counts = matrix(0, nrow = gorder(graph), ncol = n)

#recompute counts into infection potential counts

for (i in 1:gorder(graph)) {
    inf_counts[i, ] = colSums(counts[adj[[i]], , drop = FALSE]) + counts[i, ]
}

#creates matrix of repeated rows
mat_ende_lambda <- matrix(ende_lambda, nrow = ord, ncol = N, byrow = TRUE)
#creates matrix of lambdas from estimated lambda row
mat_lambdas <- matrix(epi_lambda, nrow = ord, ncol = N, byrow = TRUE)

#compute the log-likelihood
log_lik <- sum(dpois(counts[ ,2:N], #data is for 2:N, first column is an initial value
                       inf_counts[ ,1:N - 1]*mat_lambdas[, 1:N - 1] + mat_ende_lambda[, 1:N-1],
                       log = TRUE))
  
log_lik

```


# Exponential random graph model




# Appendix

## Basic simulation code


## Basic

```{r}
n = 50 #total epochs/time steps
ende_lambda = 0.5
epi_lambda = 0.5
init = 0



counts = rep(0, n)
counts[1] = init #initial value




for (i in 1:n) {
  ende = rpois(1, ende_lambda)
  epi = rpois(1, epi_lambda*counts[i])
  counts[i+1] = ende + epi
}

print(counts)
  
```


## vector lambda values

```{r}
n = 50 #total epochs/time steps
ende_lambda = rep(0.5, n) #vector of endemic lambda values (\nu)
epi_lambda = rep(0.5, n)  #vector of epidemic lambda values
init = 1   #Z_0 value is the initial number of people infected



counts = rep(0, n) #initialize count vector
counts[1] = init #initial value


for (i in 1:n) {
  ende = rpois(1, ende_lambda[i])
  epi = rpois(1, epi_lambda[i]*counts[i])
  
  counts[i+1] = ende + epi
}

```


# Surveillance Package


```{r}
library(surveillance)
load("stsEHEC.RData")
plot(rowSums(sts@observed), type = "l")
grep("nich", colnames(sts@observed))
observed <- sts@observed
dim(observed)
                    
observed[535:570,c(5:20,326,399)]
apply(observed, 2, max)
```

* epoch: time step (vector length 574)
* freq: integer
* start: date (vector)
* observed: counts? (array 574, 412) each row is a time step, each column is a location
* state: vector 412
* neighborhood: adjaceny matrix or neighborhood orders
* populationFractions


# Citations